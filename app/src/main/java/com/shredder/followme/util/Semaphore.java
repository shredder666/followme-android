package com.shredder.followme.util;

public class Semaphore {

	// init as true to allow the first caller of release to have it
	private boolean signal = true;

	public synchronized void take() {
		// log.d("takeStart");
		this.signal = true;
		this.notify();
		// log.d("takeEnd");
	}

	public synchronized void release() throws InterruptedException {
		// log.d("releaseStart");
		while (!this.signal)
			wait();
		this.signal = false;
		// log.d("releaseEnd");
	}

}