package com.shredder.followme.util;

import android.content.Context;
import android.provider.Settings;

import com.shredder.followme.FollowMeApplication;

public class UniqueId {
    public static String createUniqueDeviceId(Context context) {
        return Settings.Secure.getString(FollowMeApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
