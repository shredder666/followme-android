package com.shredder.followme.base;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.shredder.common.eventbus.ui.EventReportingFragment;
import com.shredder.followme.util.StringUtil;

public abstract class GAEventReportingFragment extends EventReportingFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        String screenName = getScreenName();
        if (StringUtil.isNullOrEmpty(screenName)) {
            return;
        }
        if (activity == null) {
            return;
        }
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "SCREEN");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, screenName);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
    }

    public abstract String getScreenName();
}
