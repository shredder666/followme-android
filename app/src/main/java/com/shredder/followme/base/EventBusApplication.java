package com.shredder.followme.base;

import android.app.Application;

import com.shredder.followme.FollowEventBusRegistry;

public class EventBusApplication extends Application {

    public static EventBusApplication instance;

    public static EventBusApplication getInstance() {
        return instance;
    }

    private FollowEventBusRegistry mRegistry;

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
        startEventProcessing();
    }

    private void startEventProcessing() {
        mRegistry = new FollowEventBusRegistry(this);
        mRegistry.registerDefaultSubscribers();
    }

    private void stopEventProcessing() {
        instance = null;
        mRegistry.unregisterAllSubscribers();
        mRegistry = null;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        stopEventProcessing();
    }
}
