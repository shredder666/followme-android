package com.shredder.followme;

import com.crashlytics.android.Crashlytics;
import com.shredder.followme.base.EventBusApplication;

import io.fabric.sdk.android.Fabric;

public class FollowMeApplication extends EventBusApplication {

    private static FollowMeApplication instance;

    public static FollowMeApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }

    public static String getStringResource(int resId) {
        return instance.getResources().getString(resId);
    }
}
