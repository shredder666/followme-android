package com.shredder.followme;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.shredder.common.eventbus.event.TransactionFragmentEvent;
import com.shredder.followme.base.BaseActivity;
import com.shredder.followme.plugin.map.FmMapFragment;
import com.shredder.followme.plugin.navigation.NavigationFragment;
import com.shredder.followme.receiver.OnActivityIntentBroadcastReceiver;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainActivityView {
    private static final String TAG = "MainActivity";
    public static final int PERMISSIONS_REQUEST_CODE = 5468;

    MainActivityPresenter mMainPresenter;
    private static MainActivity instance;
    private OnActivityIntentBroadcastReceiver intentListener;

    public static MainActivity getInstance() {
        return instance;
    }

    @BindView(R.id.main_drawer) DrawerLayout mDrawerLayout;
    @BindView(R.id.main_toolbar) Toolbar toolbar;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        instance = this;
        setupDrawerAndToggle();
        mMainPresenter.start(this);
        intentListener = new OnActivityIntentBroadcastReceiver();
        registerReceiver(intentListener, intentListener.getIntentFilter());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mMainPresenter.permissionsUpdated(hasLocationPermissions());
    }

    private void setupDrawerAndToggle() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setDrawerIndicatorEnabled(true);
            }
        };
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!hasLocationPermissions()) {
            requestPermissions();
        }else{
            mMainPresenter.permissionsUpdated(hasLocationPermissions());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(intentListener);
        mMainPresenter.stop();
        instance = null;
    }

    @Override
    protected ActionBarDrawerToggle getDrawerToggle() {
        return drawerToggle;
    }

    @Override
    protected DrawerLayout getDrawer() {
        return mDrawerLayout;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.hasExtra("group")) {
            String group = intent.getStringExtra("group");
            mMainPresenter.changeGroup(group);
        }
    }

    private void initialisePresenter() {
        FollowEventBusRegistry.getInstance().registerSubscriber(this);
        mMainPresenter = new MainActivityPresenter();
    }

    @Override
    protected int getContainerId(TransactionFragmentEvent event) {
        Class<? extends Fragment> fragmentClass = event.getFragmentClass();
        if (fragmentClass.equals(NavigationFragment.class)) {
            return R.id.main_navigation;
        }
        if (fragmentClass.equals(FmMapFragment.class)) {
            return R.id.map_content;
        }
        return R.id.main_content;
    }

    private boolean hasLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void closeDrawer() {
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
