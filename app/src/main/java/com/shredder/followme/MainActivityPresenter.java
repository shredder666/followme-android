package com.shredder.followme;

import com.shredder.common.eventbus.presenter.EventListeningPresenter;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.NewUserJoinedGroupEvent;
import com.shredder.followme.event.PermissionsUpdatedEvent;
import com.shredder.followme.event.ServiceConnectedRequestEvent;
import com.shredder.followme.event.show.ShowSettingsEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

class MainActivityPresenter extends EventListeningPresenter {

    private MainActivityView view;

    public void changeGroup(String group) {
        post(new ConfigGroupEvent(group));
    }

    public void start(MainActivityView view) {
        this.view = view;
        startEventListening();
        post(new ServiceConnectedRequestEvent());
    }

    public void stop() {
        stopEventListening();
        this.view = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShowSettingsEvent event) {
        view.closeDrawer();
    }

    public void permissionsUpdated(boolean granted) {
        postSticky(new PermissionsUpdatedEvent(granted));
    }

    @Subscribe
    public void onEvent(NewUserJoinedGroupEvent event){
        view.showToast( event.getUserUpdate().getUserName() + " just joined your group");
    }
}
