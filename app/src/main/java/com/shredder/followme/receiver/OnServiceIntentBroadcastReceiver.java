package com.shredder.followme.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.shredder.followme.builder.IntentBuilder;
import com.shredder.followme.service.FollowMeService;
import com.shredder.followme.service.plugincontroller.LocationSyncPluginController;

public class OnServiceIntentBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "OnServiceIntent";
    private final LocationSyncPluginController locationSyncPluginController;

    public OnServiceIntentBroadcastReceiver(LocationSyncPluginController locationSyncPluginController) {
        this.locationSyncPluginController = locationSyncPluginController;
    }

    public IntentFilter getIntentFilter() {
        return IntentBuilder.createServiceIntentFilter();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent: " + intent.getAction());
        if (!getIntentFilter().hasAction(intent.getAction())) {
            Log.e(TAG, "Unknown intent found: " + intent.getAction());
        }
        String action = intent.getAction();
        switch (action) {
            case IntentBuilder.ACTION_CONFIG:
                postConfigEvent(intent.getExtras());
                break;
            case IntentBuilder.ACTION_STOP:
                locationSyncPluginController.stop();
                context.stopService(new Intent(context, FollowMeService.class));
                break;
            case IntentBuilder.ACTION_PING:
                if (!IntentBuilder.isPingResponse(intent)) {
                    context.sendBroadcast(IntentBuilder.createPingResponseIntent());
                }
                break;
            default:
                break;
        }
    }

    private void postConfigEvent(Bundle extras) {
        if (extras.containsKey(IntentBuilder.EXTRA_NAME)) {
            locationSyncPluginController.setName(extras.getString(IntentBuilder.EXTRA_NAME));
        }
        if (extras.containsKey(IntentBuilder.EXTRA_GROUP)) {
            locationSyncPluginController.setGroup(extras.getString(IntentBuilder.EXTRA_GROUP));
        }
    }
}
