package com.shredder.followme.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.shredder.common.eventbus.ShEventBus;
import com.shredder.followme.builder.IntentBuilder;
import com.shredder.followme.event.IntentReceivedEvent;

public class OnActivityIntentBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "OnActivityIntent";

    public IntentFilter getIntentFilter() {
        return IntentBuilder.createActivityIntentFilter();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent: " + intent.getAction());
        if (!getIntentFilter().hasAction(intent.getAction())) {
            Log.e(TAG, "Unknown intent found: " + intent.getAction());
        }
        ShEventBus.postEvent(new IntentReceivedEvent(intent));
    }
}
