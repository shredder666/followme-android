package com.shredder.followme.event;

import lombok.Data;

@Data
public class DismissFeedbackMessageEvent {
    private final String feedbackMessageTag;
}
