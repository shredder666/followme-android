package com.shredder.followme.event;

import android.content.Intent;

import lombok.Data;

@Data
public class IntentReceivedEvent {
    private final Intent intent;
}
