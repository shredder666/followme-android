package com.shredder.followme.event.sticky;

import android.location.Location;

import lombok.Data;

@Data
public class LocationUpdatedEvent {
    private final Location location;
    private final boolean locationAvailable;

    public LocationUpdatedEvent(Location location ) {
        this.locationAvailable = (location != null);
        this.location = location;
    }
}