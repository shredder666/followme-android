package com.shredder.followme.event;

import lombok.Data;

@Data
public class GroupDoShareEvent {
    private final String group;
}
