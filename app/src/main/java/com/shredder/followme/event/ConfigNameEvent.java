package com.shredder.followme.event;

import lombok.Data;

@Data
public class ConfigNameEvent {
    private final String name;
}
