package com.shredder.followme.event;

import lombok.Data;

@Data
public class PermissionsUpdatedEvent {
    private final boolean granted;
}
