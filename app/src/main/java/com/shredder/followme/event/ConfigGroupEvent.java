package com.shredder.followme.event;

import lombok.Data;

@Data
public class ConfigGroupEvent {
    private final String group;
}
