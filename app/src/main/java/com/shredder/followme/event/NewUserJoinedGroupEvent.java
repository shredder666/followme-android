package com.shredder.followme.event;

import com.shredder.followme.model.UserUpdate;

import lombok.Data;

@Data
public class NewUserJoinedGroupEvent {
    private final UserUpdate userUpdate;
}
