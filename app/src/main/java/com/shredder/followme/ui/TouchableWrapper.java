package com.shredder.followme.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

    private onTouchUp mTouchActionUp;

    public TouchableWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchableWrapper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                fireTouchUp(event);
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    private void fireTouchUp(MotionEvent event) {
        if (mTouchActionUp != null) {
            mTouchActionUp.onTouchUp(event);
        }
    }

    public void setTouchUpAction(onTouchUp upAction){
        mTouchActionUp = upAction;
    }

    public interface onTouchUp {
        public void onTouchUp(MotionEvent event);
    }
}