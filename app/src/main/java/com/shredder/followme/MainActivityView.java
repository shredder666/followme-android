package com.shredder.followme;

interface MainActivityView {
    void closeDrawer();

    void showToast(String message);
}
