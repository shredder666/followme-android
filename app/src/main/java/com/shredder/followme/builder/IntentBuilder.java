package com.shredder.followme.builder;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.service.FollowMeService;


public class IntentBuilder {
    public static final String PACKAGE_NAME = "com.shredder.followme.";

    //Control
    public static final String ACTION_STOP = PACKAGE_NAME + "ACTION_STOP";
    public static final String ACTION_CONFIG = PACKAGE_NAME + "ACTION_CONFIG";
    public static final String ACTION_UPDATE = PACKAGE_NAME + "ACTION_UPDATE";
    public static final String ACTION_PING = PACKAGE_NAME + "ACTION_PING";

    //Extras
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_GROUP = "EXTRA_GROUP";
    public static final String EXTRA_USER_UPDATE = "EXTRA_USER_UPDATE";
    public static final String EXTRA_PING = "EXTRA_PING";

    public static Intent createStartServiceIntent(Context context, String group, String name) {
        Intent intent = new Intent(context, FollowMeService.class);
        intent.putExtra(EXTRA_GROUP, group);
        intent.putExtra(EXTRA_NAME, name);
        return intent;
    }

    public static Intent createStopServiceIntent(Context context) {
        Intent intent = new Intent(context, FollowMeService.class);
        return intent;
    }

    public static Intent createStopIntent() {
        Intent intent = new Intent(ACTION_STOP);
        return intent;
    }

    public static Intent createConfigNameIntent(String name) {
        Intent intent = new Intent(ACTION_CONFIG);
        intent.putExtra(EXTRA_NAME, name);
        return intent;
    }

    public static Intent createConfigGroupIntent(String group) {
        Intent intent = new Intent(ACTION_CONFIG);
        intent.putExtra(EXTRA_GROUP, group);
        return intent;
    }

    public static Intent createUpdateGroupIntent(UserUpdate userUpdate) {
        Intent intent = new Intent(ACTION_UPDATE);
        intent.putExtra(EXTRA_USER_UPDATE, userUpdate);
        return intent;
    }

    public static IntentFilter createServiceIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_CONFIG);
        filter.addAction(ACTION_STOP);
        filter.addAction(ACTION_PING);
        return filter;
    }

    public static IntentFilter createActivityIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE);
        filter.addAction(ACTION_STOP);
        filter.addAction(ACTION_PING);
        return filter;
    }

    public static Intent createPingRequestIntent() {
        return createPingIntent(true);
    }

    public static Intent createPingResponseIntent() {
        return createPingIntent(false);
    }

    private static Intent createPingIntent(boolean isRequest) {
        Intent intent = new Intent(ACTION_PING);
        intent.putExtra(EXTRA_PING, isRequest);
        return intent;
    }

    public static boolean isPingResponse(Intent intent) {
        boolean isRequest = intent.getBooleanExtra(IntentBuilder.EXTRA_PING, true);
        return !isRequest;
    }
}
