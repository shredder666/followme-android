package com.shredder.followme.plugin.map.presenter.map;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.plugin.map.FmMapView;

import java.util.Iterator;
import java.util.Map;

public class CircleRegistry extends MapRegistry<Circle> {
    private static final int strokeColor = 0xFF0000FF; //blue outline
    private static final int shadeColor = 0x110000FF; //blue shade

    public CircleRegistry(FmMapView view) {
        super(view);
    }

    @Override
    public void clear() {
        for (Iterator<Map.Entry<String, Circle>> it = registry.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Circle> entry = it.next();
            Circle circle = entry.getValue();
            circle.remove();
            it.remove();
        }
    }

    @Override
    public void update(UserUpdate userUpdate) {
        if (isNewUser(userUpdate)) {
            addCircle(userUpdate);
        } else {
            updateCircle(userUpdate);
        }
    }

    private void updateCircle(UserUpdate userUpdate) {
        Circle userCircle = registry.get(userUpdate.getUniqueId());
        LatLng position = new LatLng(userUpdate.getLatitude(), userUpdate.getLongitude());
        userCircle.setCenter(position);
        userCircle.setRadius(userUpdate.getAccuracy());
    }

    private void addCircle(UserUpdate user) {
        LatLng position = new LatLng(user.getLatitude(), user.getLongitude());
        CircleOptions co = new CircleOptions();
        co.center(position);
        co.radius(user.getAccuracy());
        co.fillColor(shadeColor);
        co.strokeColor(strokeColor);
        co.strokeWidth(4.0f);
        registry.put(user.getUniqueId(), view.addCircle(co));
    }
}
