package com.shredder.followme.plugin.map;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public interface FmMapView {
    void goToPosition(LatLng center, float zoom, boolean animate);
    Marker addMarker(MarkerOptions markerOptions);
    Circle addCircle(CircleOptions circleOptions);
    void animateToBounds(LatLngBounds bounds);
    void setFitToWindowVisibility(boolean show);
    void setPlayStopButtonVisibility(boolean show);
    void setButtonToPlay(boolean showPlay);
    void setMyLocationAvailable(boolean available);
    void showStartNotification();
    void showStopNotification();
    void showFitButtonHint();
}
