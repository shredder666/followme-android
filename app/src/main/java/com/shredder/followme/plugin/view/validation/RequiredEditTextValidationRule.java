package com.shredder.followme.plugin.view.validation;

import com.shredder.followme.R;
import com.shredder.followme.plugin.view.CustomTextInputLayout;

import org.apache.commons.lang3.StringUtils;

public class RequiredEditTextValidationRule extends EditTextValidationRule {

    public RequiredEditTextValidationRule(final CustomTextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @Override
    public boolean validate() {
        return StringUtils.isNotEmpty(editText.getText().toString().trim());
    }

    @Override
    public String getErrorMessage() {
        return textInputLayout.getResources().getString(R.string.error_message_required, textInputLayout.getHint());
    }
}
