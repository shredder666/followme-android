package com.shredder.followme.plugin.map.presenter;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.shredder.common.eventbus.presenter.EventListeningPresenter;
import com.shredder.common.eventbus.presenter.PresenterHierarchy;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ConfigNameEvent;
import com.shredder.followme.event.ControlPlayEvent;
import com.shredder.followme.event.ControlStopEvent;
import com.shredder.followme.event.PermissionsUpdatedEvent;
import com.shredder.followme.event.ServiceConnectedResponseEvent;
import com.shredder.followme.event.sticky.LocationUpdatedEvent;
import com.shredder.followme.model.FollowMePreferences;
import com.shredder.followme.model.MapPreferences;
import com.shredder.followme.plugin.map.FmMapView;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MapMasterPresenter extends EventListeningPresenter {

    private final PresenterHierarchy<FmMapView> presenterHierarchy;
    private final MapCameraPresenter mapCameraPresenter;
    private final FmMapPresenter fmMapPresenter;
    private String name;
    private String group;
    private FmMapView view;
    private LatLng ownLocation;
    private final FollowMePreferences preferences;

    public MapMasterPresenter(FollowMePreferences preferences) {
        this.preferences = preferences;
        mapCameraPresenter = new MapCameraPresenter();
        fmMapPresenter = new FmMapPresenter();
        presenterHierarchy = new PresenterHierarchy<>(
                mapCameraPresenter,
                new FmMapPresenter(),
                fmMapPresenter
        );
    }

    public void start(FmMapView view) {
        this.view = view;
        presenterHierarchy.start(view);
        startEventListening();
    }

    public void stop() {
        presenterHierarchy.stop();
        stopEventListening();
        this.view = null;
    }

    public void onOwnLocationButtonClick() {
        mapCameraPresenter.onOwnLocationButtonClick();
    }

    public void saveCameraPosition(float zoom, LatLngBounds bounds) {
        MapPreferences.getInstance().setLastLocation(bounds.getCenter());
        MapPreferences.getInstance().setLastBounds(bounds);
        MapPreferences.getInstance().setLastZoomLevel(zoom);
    }

    public void onUserInteraction() {
        mapCameraPresenter.onUserInteraction();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigNameEvent event) {
        name = event.getName();
        view.setPlayStopButtonVisibility(configIsValid());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigGroupEvent event) {
        group = event.getGroup();
        view.setPlayStopButtonVisibility(configIsValid());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ControlStopEvent event) {
        view.setPlayStopButtonVisibility(configIsValid());
        view.setButtonToPlay(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ServiceConnectedResponseEvent event) {
        view.setPlayStopButtonVisibility(configIsValid());
        view.setButtonToPlay(false);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(PermissionsUpdatedEvent event) {
        boolean granted = event.isGranted();
        view.setFitToWindowVisibility(granted);
        view.setMyLocationAvailable(granted);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(LocationUpdatedEvent event) {
        ownLocation = new LatLng(event.getLocation().getLatitude(), event.getLocation().getLongitude());
    }

    private boolean configIsValid() {
        return StringUtils.isNotBlank(group) && StringUtils.isNotBlank(name);
    }

    public void stopClicked() {
        post(new ControlStopEvent());
        view.showStopNotification();
        view.setButtonToPlay(true);
    }

    public void startClicked() {
        post(new ControlPlayEvent());
        view.showStartNotification();
        view.setButtonToPlay(false);
    }

    public void fitClicked() {
        LatLngBounds window = fmMapPresenter.getAllMarkerBounds();
        if (window != null) {
            LatLngBounds boundsIncludingOwnLocation = LatLngBounds.builder()
                    .include(window.northeast)
                    .include(window.southwest)
                    .include(ownLocation)
                    .build();
            mapCameraPresenter.animateToBounds(boundsIncludingOwnLocation);
        } else {
            mapCameraPresenter.animateToMyLocation();
        }

        if (!shouldShowHint()) {
            return;
        }
        showHint();
    }

    private boolean shouldShowHint() {
        if (preferences.hasShownFitButtonHint()) {
            return false;
        }
        preferences.setHasShownFitButtonHint();
        return true;
    }

    private void showHint() {
        view.showFitButtonHint();
    }
}
