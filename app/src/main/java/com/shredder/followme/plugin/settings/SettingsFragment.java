package com.shredder.followme.plugin.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.shredder.common.eventbus.ui.EventReportingFragment;
import com.shredder.common.util.KeyboardUtils;
import com.shredder.followme.BuildConfig;
import com.shredder.followme.R;
import com.shredder.followme.base.BackButtonSupportFragment;
import com.shredder.followme.plugin.view.CustomTextInputLayout;
import com.shredder.followme.plugin.view.validation.FormValidatorSet;
import com.shredder.followme.plugin.view.validation.RequiredEditTextValidationRule;
import com.shredder.followme.plugin.view.validation.SimpleTextValidationRule;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends EventReportingFragment implements SettingsView, BackButtonSupportFragment {

    @BindView(R.id.settings_username_edit_text) EditText userNameEditText;
    @BindView(R.id.settings_username_layout) CustomTextInputLayout userNameEditTextInputLayout;
    @BindView(R.id.settings_group_edit_text) EditText groupEditText;
    @BindView(R.id.settings_group_layout) CustomTextInputLayout groupEditTextInputLayout;
    @BindView(R.id.fragment_settings_version_text) TextView versionText;

    private SettingsPresenter mPresenter;
    private FormValidatorSet mFormValidator;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new SettingsPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFormValidator = new FormValidatorSet();
        mFormValidator.add(new RequiredEditTextValidationRule(userNameEditTextInputLayout));
        mFormValidator.add(new RequiredEditTextValidationRule(groupEditTextInputLayout));
        mFormValidator.add(new SimpleTextValidationRule(userNameEditTextInputLayout));
        mFormValidator.add(new SimpleTextValidationRule(groupEditTextInputLayout));
        mPresenter.start(this);
        versionText.setText("v" + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    @Override
    public void setName(String name) {
        userNameEditText.setText(name);
    }

    @Override
    public void setGroup(String group) {
       groupEditText.setText(group);
    }

    @Override
    public boolean onBackPressed() {
        if (!mFormValidator.validateAll()) {
            return true;
        }
        mPresenter.onContinueClicked(userNameEditText.getText().toString(), groupEditText.getText().toString());
        KeyboardUtils.hideSoftKeyboard(getActivity());
        return false;
    }
}
