package com.shredder.followme.plugin.map.presenter;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shredder.common.eventbus.presenter.EventListeningChildPresenter;
import com.shredder.followme.event.sticky.LocationUpdatedEvent;
import com.shredder.followme.plugin.map.FmMapView;

import org.greenrobot.eventbus.Subscribe;

class MapOwnLocationMarkerPresenter extends EventListeningChildPresenter<FmMapView> {

    private Marker ownLocationMarker;

    private void updateLocationMarker(LocationUpdatedEvent event) {
        if (!event.isLocationAvailable()) {
            hideLocationMarkerIfShown();
            return;
        }
        if (ownLocationMarker == null) {
            createNewLocationMarker(event.getLocation());
            return;
        }
        ownLocationMarker.setPosition(new LatLng(event.getLocation().getLatitude(), event.getLocation().getLongitude()));
    }

    private void createNewLocationMarker(Location location) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ico_current_position));
        ownLocationMarker = view.addMarker(markerOptions);
    }

    public void hideLocationMarkerIfShown() {
        if (ownLocationMarker == null) {
            return;
        }
        ownLocationMarker.remove();
    }

    @Subscribe
    public void onLocationUpdated(LocationUpdatedEvent event) {
        updateLocationMarker(event);
    }
}
