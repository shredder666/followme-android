package com.shredder.followme.plugin.view.validation;

import android.support.design.widget.TextInputLayout;

public interface  FormValidationRule {
    TextInputLayout getTextInputLayout();
    boolean validate();
    String getErrorMessage();
}
