package com.shredder.followme.plugin.map.presenter;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.shredder.common.eventbus.presenter.EventListeningChildPresenter;
import com.shredder.followme.event.sticky.LocationUpdatedEvent;
import com.shredder.followme.model.MapPreferences;
import com.shredder.followme.plugin.map.FmMapView;

import org.greenrobot.eventbus.Subscribe;

import lombok.Getter;

class MapCameraPresenter extends EventListeningChildPresenter<FmMapView> {

    private boolean cameraShouldFollowCurrentLocation = true;
    @Getter
    private Location ownLocation;

    public MapCameraPresenter() {
    }

    @Override
    public void start(FmMapView view) {
        super.start(view);
        if (cameraShouldFollowCurrentLocation) {
            moveCameraToLastPosition();
        }
    }

    public void animateToBounds(LatLngBounds bounds){
        cameraShouldFollowCurrentLocation = false;
        view.animateToBounds(bounds);
    }

    private void moveCameraToLastPosition() {
        LatLng lastLocation = MapPreferences.getInstance().getLastLocation();
        float lastZoomLevel = MapPreferences.getInstance().getLastZoomLevel();
        view.goToPosition(lastLocation, lastZoomLevel, false);
    }

    private void animateToLocation(LatLng location) {
        view.goToPosition(location, MapPreferences.getInstance().getLastZoomLevel(), true);
    }

    public void animateToMyLocation() {
        if (ownLocation == null) {
            return;
        }
        cameraShouldFollowCurrentLocation = false;
        moveCameraToOwnLocationEvent();
    }

    public void onOwnLocationButtonClick() {
        if (ownLocation == null) {
            return;
        }
        cameraShouldFollowCurrentLocation = true;
        moveCameraToOwnLocationEvent();
    }

    private void moveCameraToOwnLocationEvent() {
        LatLng coordinates = new LatLng(ownLocation.getLatitude(), ownLocation.getLongitude());
        animateToLocation(coordinates);
    }

    @Subscribe
    public void onLocationUpdated(LocationUpdatedEvent event) {
        if (!event.isLocationAvailable()) {
            return;
        }
        ownLocation = event.getLocation();
        if (!cameraShouldFollowCurrentLocation) {
            return;
        }
        moveCameraToOwnLocationEvent();
    }

    public void onUserInteraction() {
        cameraShouldFollowCurrentLocation = false;
    }
}
