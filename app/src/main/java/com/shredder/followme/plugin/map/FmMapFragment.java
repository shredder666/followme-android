package com.shredder.followme.plugin.map;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shredder.common.util.KeyboardUtils;
import com.shredder.followme.R;
import com.shredder.followme.base.GAEventReportingFragment;
import com.shredder.followme.model.FollowMePreferences;
import com.shredder.followme.plugin.map.presenter.MapMasterPresenter;
import com.shredder.followme.ui.TouchableWrapper;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FmMapFragment extends GAEventReportingFragment implements FmMapView {
    private static final String TAG_MAP_FRAGMENT = "tag_map_fragment";

    @BindView(R.id.fragment_map_container) TouchableWrapper mMapContainer;
    @BindView(R.id.fab_container) ViewGroup fabContainer;
    @BindView(R.id.fab_playpause) ImageButton fabActionPlay;
    @BindView(R.id.fab_fit) ImageButton fabActionFit;

    @BindDrawable(R.drawable.ic_play_arrow_white_36dp) Drawable playDrawable;
    @BindDrawable(R.drawable.ic_stop_white_36dp) Drawable stopDrawable;

    private GoogleMap map;
    private MapMasterPresenter mPresenter;
    private boolean isRunning; //TODO save this in the bundle for restoration

    TouchableWrapper.onTouchUp mapTouchUpAction = new TouchableWrapper.onTouchUp() {

        @Override
        public void onTouchUp(MotionEvent event) {
            mPresenter.onUserInteraction();
        }
    };

    private GoogleMap.OnMarkerClickListener onMarkerClickedListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
            } else {
                marker.showInfoWindow();
            }
            return true;
        }
    };

    public static FmMapFragment newInstance() {
        return new FmMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MapMasterPresenter(new FollowMePreferences(getActivity()));
        isStarting = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        addGoogleMapToView();
        ButterKnife.bind(this, view);
        KeyboardUtils.hideSoftKeyboard(getActivity());
        return view;
    }

    private void addGoogleMapToView() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_map_container, SupportMapFragment.newInstance(), TAG_MAP_FRAGMENT).commit();
    }

    boolean isStarting;

    @Override
    public void onStart() {
        super.onStart();
        if (map == null) {
            isStarting = true;
            createMap();

        } else {
            if (!isStarting) {
                mPresenter.start(this);
            }
        }
        hookMapTouchEvents();
    }

    private void onMapLoaded() {
        mPresenter.start(this);
        isStarting = false;
    }

    private void onMapCameraChanged() {
        saveMapBounds();
    }

    private void saveMapBounds() {
        LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
        float zoom = map.getCameraPosition().zoom;
        mPresenter.saveCameraPosition(zoom, bounds);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    private void hookMapTouchEvents() {
        mMapContainer.setTouchUpAction(mapTouchUpAction);
    }

    private void createMap() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentByTag(TAG_MAP_FRAGMENT)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.getUiSettings().setZoomControlsEnabled(false);
                map.getUiSettings().setRotateGesturesEnabled(false);
                map.getUiSettings().setTiltGesturesEnabled(false);
                map.getUiSettings().setIndoorLevelPickerEnabled(false);
                map.getUiSettings().setCompassEnabled(false);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        mPresenter.onOwnLocationButtonClick();//TODO fix
                        return true;
                    }
                });
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map.setOnMarkerClickListener(onMarkerClickedListener);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        FmMapFragment.this.onMapLoaded();
                    }
                });
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        onMapCameraChanged();
                    }
                });
            }
        });
    }

    @Override
    public void goToPosition(LatLng center, float zoom, boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(center, zoom);
        if (animate) {
            map.animateCamera(cameraUpdate);
        } else {
            map.moveCamera(cameraUpdate);
        }
    }

    @Override
    public Marker addMarker(MarkerOptions markerOptions) {
        return map.addMarker(markerOptions);
    }

    @Override
    public Circle addCircle(CircleOptions circleOptions) {
        return map.addCircle(circleOptions);
    }

    @Override
    public void animateToBounds(LatLngBounds bounds) {
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    @Override
    public void setMyLocationAvailable(boolean available) {
        map.setMyLocationEnabled(available);
    }

    @Override
    public void setFitToWindowVisibility(boolean show) {
        fabActionFit.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.fab_playpause)
    public void onClick(View v) {
        if (isRunning) {
            mPresenter.stopClicked();
            isRunning = false;
        } else {
            mPresenter.startClicked();
            isRunning = true;
        }
    }

    @Override
    public void showStartNotification() {
        Toast.makeText(getActivity(), R.string.location_sharing_started, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showStopNotification() {
        Toast.makeText(getActivity(), R.string.location_sharing_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFitButtonHint() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_fit_hint)
                .setTitle(R.string.dialog_fit_hint_title)
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void setPlayStopButtonVisibility(boolean show) {
        fabActionPlay.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setButtonToPlay(boolean showPlay) {
        fabActionPlay.setImageDrawable(showPlay ? playDrawable : stopDrawable);
    }

    @OnClick(R.id.fab_fit)
    public void onFitClicked() {
        mPresenter.fitClicked();
    }

    @Override
    public String getScreenName() {
        return FmMapFragment.class.getSimpleName();
    }
}
