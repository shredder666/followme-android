package com.shredder.followme.plugin.view.validation;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.shredder.followme.plugin.view.CustomTextInputLayout;

import lombok.Data;

@Data
public abstract class EditTextValidationRule implements FormValidationRule {

    protected final CustomTextInputLayout textInputLayout;
    protected final EditText editText;

    public EditTextValidationRule(final CustomTextInputLayout textInputLayout) {
        this.textInputLayout = textInputLayout;
        this.editText = textInputLayout.getEditText();
        assert editText != null;
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputLayout.setErrorEnabled(false);
                textInputLayout.setError(null);
            }
        });
    }
}
