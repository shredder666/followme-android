package com.shredder.followme.plugin.navigation;

import com.shredder.common.recycler.DefaultListItem;

import java.util.List;

interface NavigationView {
    void setUsername(String username);
    void setGroup(String group);
    void setMenuItems(List<DefaultListItem> items);
}
