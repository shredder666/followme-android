package com.shredder.followme.plugin.view.validation;

import android.view.View;

import java.util.ArrayList;

public class FormValidatorSet extends ArrayList<FormValidationRule> {
    public boolean validateAll() {
        FormValidationRule firstHit = null;
        for (FormValidationRule validationRule : this) {
            if (validationRule.getTextInputLayout().getVisibility() == View.GONE) {
                continue;
            }
            if (!validationRule.validate()) {
                if (firstHit == null) {
                    firstHit = validationRule;
                }
                validationRule.getTextInputLayout().setErrorEnabled(true);
                validationRule.getTextInputLayout().setError(validationRule.getErrorMessage());
            } else {
                validationRule.getTextInputLayout().setErrorEnabled(false);
                validationRule.getTextInputLayout().setError(null);
            }
        }
        if (firstHit == null) {
            return true;
        }
        //firstHit.getEditText().requestFocus();
        return false;
    }
}
