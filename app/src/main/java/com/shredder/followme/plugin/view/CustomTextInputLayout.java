package com.shredder.followme.plugin.view;

import android.content.Context;
import android.graphics.Canvas;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import org.apache.commons.lang3.StringUtils;

/**
 * This is an interim solution to fix https://code.google.com/p/android/issues/detail?id=175228
 * Please use default TextInputLayout class as soon as this bug has been fixed
 */
public class CustomTextInputLayout extends TextInputLayout {

    private boolean mIsHintSet;
    private CharSequence mHint;

    public CustomTextInputLayout(Context context) {
        super(context);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (child instanceof EditText) {
            mHint = ((EditText) child).getHint();
        } else if (child instanceof Spinner) {
            mHint = ((Spinner) child).getPrompt();
        }
        super.addView(child, index, params);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!mIsHintSet && ViewCompat.isLaidOut(this)) {
            setHint(null);
            CharSequence currentEditTextHint = null;
            if (getChildAt(0) instanceof EditText) {
                currentEditTextHint = ((EditText) getChildAt(0)).getHint();
            } else if (getChildAt(0) instanceof Spinner) {
                currentEditTextHint = ((Spinner) getChildAt(0)).getPrompt();
            }
            if (currentEditTextHint != null && StringUtils.isNotEmpty(currentEditTextHint.toString())) {
                mHint = currentEditTextHint;
            }
            setHint(mHint);
            mIsHintSet = true;
        }
    }

    public CharSequence getHint() {
        return mHint;
    }

}
