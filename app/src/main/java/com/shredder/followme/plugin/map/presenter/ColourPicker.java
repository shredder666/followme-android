package com.shredder.followme.plugin.map.presenter;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class ColourPicker {
    private float[] palette;

    public ColourPicker() {
        palette = new float[]{
                BitmapDescriptorFactory.HUE_RED,
                BitmapDescriptorFactory.HUE_AZURE,
                BitmapDescriptorFactory.HUE_BLUE,
                BitmapDescriptorFactory.HUE_CYAN,
                BitmapDescriptorFactory.HUE_GREEN,
                BitmapDescriptorFactory.HUE_MAGENTA,
                BitmapDescriptorFactory.HUE_ORANGE,
                BitmapDescriptorFactory.HUE_YELLOW
        };
    }

    public float getSeeded(String uniqueId) {
        return palette[uniqueId.hashCode() % (palette.length - 1)];
    }
}
