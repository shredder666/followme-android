package com.shredder.followme.plugin.map.presenter;


import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLngBounds;
import com.shredder.common.eventbus.presenter.EventListeningChildPresenter;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ControlStopEvent;
import com.shredder.followme.event.UserUpdateEvent;
import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.plugin.map.FmMapView;
import com.shredder.followme.plugin.map.presenter.map.CircleRegistry;
import com.shredder.followme.plugin.map.presenter.map.MarkerRegistry;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

class FmMapPresenter extends EventListeningChildPresenter<FmMapView> {
    private MarkerRegistry markerRegistry;
    private CircleRegistry circleRegistry;

    @Override
    public void start(FmMapView fmMapView) {
        super.start(fmMapView);
        if (markerRegistry == null) {
            markerRegistry = new MarkerRegistry(view);
            circleRegistry = new CircleRegistry(view);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UserUpdateEvent event) {
        UserUpdate update = event.getUserUpdate();
        markerRegistry.update(update);
        circleRegistry.update(update);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ControlStopEvent event) {
        clearAll();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ConfigGroupEvent event) {
        clearAll();
    }

    private void clearAll() {
        markerRegistry.clear();
        circleRegistry.clear();
    }

    @Nullable
    public LatLngBounds getAllMarkerBounds() {
        return markerRegistry.getBounds();
    }
}
