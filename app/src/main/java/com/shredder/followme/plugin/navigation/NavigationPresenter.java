package com.shredder.followme.plugin.navigation;

import android.content.Intent;

import com.shredder.common.eventbus.presenter.EventListeningPresenter;
import com.shredder.common.recycler.DefaultListItem;
import com.shredder.followme.FollowMeApplication;
import com.shredder.followme.R;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ConfigNameEvent;
import com.shredder.followme.event.GroupDoShareEvent;
import com.shredder.followme.event.show.ShowSettingsEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class NavigationPresenter extends EventListeningPresenter {
    private final NavigationView view;
    private final DefaultListItem inviteListItem;

    public NavigationPresenter(NavigationView navigationFragment) {
        this.view = navigationFragment;
        inviteListItem = DefaultListItem.builder()
                .imageResId(R.drawable.ic_add_white_36dp)
                .title("Follow Me")
                .build();
    }

    public void start() {
        startEventListening();
        List<DefaultListItem> items = new ArrayList<>();

        items.add(DefaultListItem.builder()
                .imageResId(R.drawable.ic_person_white_36dp)
                .title("Profile")
                .selectedEvent(new ShowSettingsEvent())
                .build());

        items.add(inviteListItem);
        view.setMenuItems(items);
    }

    public void stop() {
        stopEventListening();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigNameEvent event) {
        view.setUsername(event.getName());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigGroupEvent event) {
        view.setGroup(event.getGroup());
        inviteListItem.setSelectedEvent(new GroupDoShareEvent(event.getGroup()));
    }

    @Subscribe
    public void onEvent(GroupDoShareEvent event) {
        shareClicked(event.getGroup());
    }

    public void shareClicked(String group) {
        Intent shareIntent = createGroupSharingIntent(group);
        FollowMeApplication.getInstance().startActivity(shareIntent);
    }

    private Intent createGroupSharingIntent(String group) {
        String url = FollowMeApplication.getStringResource(R.string.share_url) + "?group=" + group;
        String smsBody = FollowMeApplication.getStringResource(R.string.share_sms_body);
        smsBody = smsBody + " \"" + group + "\" " + url;
        Intent shareIntent = new Intent();
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, smsBody);
        shareIntent.setType("text/plain");
        return shareIntent;
    }
}
