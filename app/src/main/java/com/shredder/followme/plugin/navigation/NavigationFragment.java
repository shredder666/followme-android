package com.shredder.followme.plugin.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shredder.common.recycler.DefaultListItem;
import com.shredder.common.recycler.DividerDecoration;
import com.shredder.common.recycler.ShredderAdapter;
import com.shredder.followme.R;
import com.shredder.followme.base.GAEventReportingFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NavigationFragment extends GAEventReportingFragment implements NavigationView {

    @BindView(R.id.navigation_group) TextView textViewGroup;
    @BindView(R.id.navigation_list) RecyclerView recyclerView;
    @BindView(R.id.navigation_user) TextView textViewUserName;
    @BindView(R.id.navigation_user_image) ImageView imageViewUser;

    private NavigationPresenter presenter;
    private ShredderAdapter adapter;

    public static NavigationFragment newInstance() {
        return new NavigationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ShredderAdapter();
        presenter = new NavigationPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        ButterKnife.bind(this, view);
        initialiseRecycler();
        presenter.start();
        return view;
    }

    private void initialiseRecycler() {
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        DividerDecoration divider = new DividerDecoration(ContextCompat.getDrawable(this.getContext(), R.drawable.divider_vertical));
        recyclerView.addItemDecoration(divider);
    }

    @Override
    public void onDestroyView() {
        presenter.stop();
        super.onDestroyView();
    }

    @Override
    public String getScreenName() {
        return "";
    }

    @Override
    public void setUsername(String username) {
        textViewUserName.setText(getResources().getString(R.string.username, username));
    }

    @Override
    public void setGroup(String group) {
        textViewGroup.setText(getResources().getString(R.string.group, group));
    }

    @Override
    public void setMenuItems(List<DefaultListItem> items) {
        adapter.setItems(items);
    }
}
