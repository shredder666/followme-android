package com.shredder.followme.plugin.settings;

public interface SettingsView {
    void setName(String name);
    void setGroup(String group);
}
