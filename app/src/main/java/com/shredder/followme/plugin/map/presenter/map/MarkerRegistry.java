package com.shredder.followme.plugin.map.presenter.map;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.plugin.map.FmMapView;
import com.shredder.followme.plugin.map.presenter.ColourPicker;
import com.shredder.followme.util.LatLngInterpolator;
import com.shredder.followme.util.MarkerAnimation;

import java.util.Iterator;
import java.util.Map;

public class MarkerRegistry extends MapRegistry<Marker> {
    private final ColourPicker colorPicker;

    public MarkerRegistry(FmMapView view) {
        super(view);
        colorPicker = new ColourPicker();
    }

    public void update(UserUpdate userUpdate) {
        if (isNewUser(userUpdate)) {
            addMarker(userUpdate);
        } else {
            updateMarker(userUpdate);
        }
    }

    private void updateMarker(UserUpdate userUpdate) {
        Marker userMarker = registry.get(userUpdate.getUniqueId());
        updateMarkerPosition(userMarker, userUpdate);
        updateMarkerTitle(userMarker, userUpdate);
    }

    private void updateMarkerPosition(Marker userMarker, UserUpdate userUpdate) {
        LatLng position = new LatLng(userUpdate.getLatitude(), userUpdate.getLongitude());
        MarkerAnimation.animateMarkerToICS(userMarker, position, new LatLngInterpolator.Spherical());
    }

    private void updateMarkerTitle(Marker userMarker, UserUpdate userUpdate) {
        userMarker.showInfoWindow();
        userMarker.setTitle(userUpdate.getUserName());
    }

    private void addMarker(UserUpdate user) {
        LatLng position = new LatLng(user.getLatitude(), user.getLongitude());
        float colorSelected = colorPicker.getSeeded(user.getUniqueId());
        MarkerOptions m = new MarkerOptions()
                .position(position)
                .draggable(false)
                .icon(BitmapDescriptorFactory.defaultMarker(colorSelected))
                .title(user.getUserName());
        Marker newMarker = view.addMarker(m);
        newMarker.showInfoWindow();
        registry.put(user.getUniqueId(), newMarker);
    }

    public void clear() {
        for (Iterator<Map.Entry<String, Marker>> it = registry.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Marker> entry = it.next();
            Marker marker = entry.getValue();
            marker.remove();
            it.remove();
        }
    }

    @Nullable
    public LatLngBounds getBounds() {
        if (registry.size() == 0) {
            return null;
        }
        LatLngBounds.Builder bc = new LatLngBounds.Builder();
        Iterator it = registry.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Marker> pair = (Map.Entry<String, Marker>) it.next();
            bc.include(pair.getValue().getPosition());
        }
        return bc.build();
    }
}
