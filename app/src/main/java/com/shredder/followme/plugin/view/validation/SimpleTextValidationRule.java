package com.shredder.followme.plugin.view.validation;

import com.shredder.followme.R;
import com.shredder.followme.plugin.view.CustomTextInputLayout;

public class SimpleTextValidationRule extends EditTextValidationRule {

    private static final String REGEX = "^[a-z0-9]{1,20}$";

    public SimpleTextValidationRule(CustomTextInputLayout textInputLayout) {
        super(textInputLayout);
     }

    @Override
    public boolean validate() {
        String text = editText.getText().toString();
        return text.matches(REGEX);
    }

    @Override
    public String getErrorMessage() {
        String errorMessage = textInputLayout.getResources().getString(R.string.error_simple_chars_only);
        return errorMessage;
    }
}
