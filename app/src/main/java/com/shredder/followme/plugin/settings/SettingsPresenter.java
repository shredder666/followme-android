package com.shredder.followme.plugin.settings;


import com.shredder.common.eventbus.presenter.EventListeningPresenter;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ConfigNameEvent;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SettingsPresenter extends EventListeningPresenter {
    private SettingsView view;

    private String oldGroup;
    private String oldName;

    public SettingsPresenter() {
    }

    public void start(SettingsView view) {
        this.view = view;
        startEventListening();
    }

    public void stop() {
        stopEventListening();
        this.view = null;
    }

    public void onContinueClicked(String name, String group) {
        if (!StringUtils.equals(oldName, name)) {
            postSticky(new ConfigNameEvent(name));
        }
        if (!StringUtils.equals(oldGroup, group)) {
            postSticky(new ConfigGroupEvent(group));
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigNameEvent event) {
        oldName = event.getName();
        view.setName(event.getName());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigGroupEvent event) {
        oldGroup = event.getGroup();
        view.setGroup(event.getGroup());
    }
}
