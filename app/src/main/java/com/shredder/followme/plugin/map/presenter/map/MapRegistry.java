package com.shredder.followme.plugin.map.presenter.map;

import com.shredder.followme.FollowMeApplication;
import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.plugin.map.FmMapView;
import com.shredder.followme.util.UniqueId;

import java.util.HashMap;

public abstract class MapRegistry<T> {
    protected final FmMapView view;
    protected final HashMap<String, T> registry = new HashMap<>();
    protected final String uniqueId;

    protected MapRegistry(FmMapView view) {
        this.view = view;
        uniqueId = UniqueId.createUniqueDeviceId(FollowMeApplication.getInstance());
    }

    public boolean isNewUser(UserUpdate userUpdate) {
        return !registry.containsKey(userUpdate.getUniqueId());
    }
    abstract void clear();
    abstract void update(UserUpdate userUpdate);
}
