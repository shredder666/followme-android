package com.shredder.followme.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.shredder.followme.builder.IntentBuilder;
import com.shredder.followme.service.manager.FollowMeNotificationHandler;

public class FollowMeService extends Service {
    private static final String TAG = "FollowMeService";
    private FollowMeNotificationHandler notificationHandler;
    private ServiceEventBusRegistry registry;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationHandler = new FollowMeNotificationHandler(this);
        registry = new ServiceEventBusRegistry(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(FollowMeNotificationHandler.ONGOING_NOTIFICATION_ID, notificationHandler.showOngoingNotification());
        Bundle extras = intent.getExtras();
        registry.start(extras.getString(IntentBuilder.EXTRA_GROUP), extras.getString(IntentBuilder.EXTRA_NAME));
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind - returning null");
        return null;
    }

    @Override
    public void onDestroy() {
        notificationHandler.cancelOngoingNotification();
        registry.stop();
        super.onDestroy();
    }
}
