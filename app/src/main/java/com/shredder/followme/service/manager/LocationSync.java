package com.shredder.followme.service.manager;

import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.shredder.followme.model.MqttPayload;
import com.shredder.followme.model.UserUpdate;
import com.shredder.location.GoogleLocationProvider;
import com.shredder.location.LocationConfig;
import com.shredder.mqtt.MqttManager;
import com.shredder.mqtt.MqttManagerConfig;

import org.apache.commons.lang3.StringUtils;

import lombok.Setter;

public class LocationSync {
    private static final String TAG = "LocationSync";
    private static final String PREFIX = "Follow/";
    private final MqttManager mqttManager;
    private final String uniqueId;
    private GoogleLocationProvider locationProvider;
    private boolean isRunning;
    private String group;

    @Setter
    private String name;
    @Setter
    private Listener onLocationReceivedListener;

    private final GoogleLocationProvider.OnLocationChangedListener onLocationChanged = new GoogleLocationProvider.OnLocationChangedListener() {
        @Override
        public void onLocationChanged(Location result) {
            publishNewLocation(result);
        }
    };
    private final MqttManager.Listener mqttListener = new MqttManager.Listener() {
        @Override
        public void onMessageReceived(String message, String topic) {
            Log.i(TAG, "received: topic(" + topic + ") message (" + message + ")");
            if (!isRunning) {
                Log.i(TAG, "ignoring - LocationSync is stopped");
                return;
            }
            messageReceived(message, topic);
        }
    };

    public LocationSync(Configuration config) {
        this.mqttManager = new MqttManager(config, mqttListener);
        uniqueId = config.getUniqueId();
    }

    public LocationSync(Configuration config, GoogleLocationProvider locationProvider) {
        this(config);
        this.locationProvider = locationProvider;
        this.locationProvider.setConfig(config);
    }

    public void start() {
        if (isRunning) {
            return;
        }
        isRunning = true;
        this.locationProvider.addLocationListener(onLocationChanged);
        this.mqttManager.start();
    }

    public void stop() {
        if (!isRunning) {
            return;
        }
        this.locationProvider.removeLocationListener();
        this.mqttManager.clearRetained(createPublishTopic());
        this.mqttManager.stop();
        isRunning = false;
    }

    public void setGroup(String newGroup) {
        this.group = newGroup;
        mqttManager.unsubscribe(createSubscriptionTopic(this.group));
        mqttManager.subscribe(createSubscriptionTopic(this.group));
    }

    private String createSubscriptionTopic(String group) {
        return PREFIX + group + "/#";
    }

    private void publishNewLocation(Location result) {
        if (!canPublish()) {
            Log.e(TAG, "Name and/or group is not yet configured. Can't publish");
            return;
        }
        Gson gson = new Gson();
        MqttPayload location = MqttPayload.builder()
                .userName(name)
                .lat(result.getLatitude())
                .lon(result.getLongitude())
                .accuracy(result.getAccuracy())
                .bearing(result.getBearing())
                .build();
        String locationString = gson.toJson(location);
        mqttManager.publish(locationString, createPublishTopic(), true);
    }

    private boolean canPublish() {
        return isRunning && StringUtils.isNotBlank(name) && StringUtils.isNotBlank(group);
    }

    private String createPublishTopic() {
        return PREFIX + group + "/" + uniqueId;
    }

    private void messageReceived(String message, String topic) {
        Gson gson = new Gson();
        MqttPayload mqttPayload;
        try {
            mqttPayload = gson.fromJson(message, MqttPayload.class);
            onLocationReceived(topic, mqttPayload);
        } catch (JsonSyntaxException e) {
            Log.i(TAG, "Ignoring badly formatted message: " + message);
        }
    }

    private void onLocationReceived(String topic, MqttPayload payload) {
        if (onLocationReceivedListener == null) {
            Log.w(TAG, "Received a payload but no one is listening. Memory leak?");
        }
        String[] splitTopic = StringUtils.split(topic, "/");
        String baseTopic = splitTopic[1];
        String uniqueId = splitTopic[2];
        UserUpdate userUpdate = UserUpdate.builder()
                .group(baseTopic)
                .userName(payload.getUserName())
                .latitude(payload.getLat())
                .longitude(payload.getLon())
                .uniqueId(uniqueId)
                .accuracy(payload.getAccuracy())
                .bearing(payload.getBearing())
                .build();

        if (StringUtils.equals(userUpdate.getUniqueId(), this.uniqueId)) {
            Log.w(TAG, "Ignoring message from self");
            return;
        }

        onLocationReceivedListener.onMessageReceived(userUpdate);
    }

    public interface Configuration extends LocationConfig, MqttManagerConfig {
    }

    public interface Listener {
        void onMessageReceived(UserUpdate userUpdate);
    }

}
