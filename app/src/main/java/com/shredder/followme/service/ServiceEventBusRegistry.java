package com.shredder.followme.service;

import android.content.Context;

import com.shredder.followme.builder.IntentBuilder;
import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.receiver.OnServiceIntentBroadcastReceiver;
import com.shredder.followme.service.manager.LocationSync;
import com.shredder.followme.service.plugincontroller.LocationSyncPluginController;
import com.shredder.followme.util.UniqueId;
import com.shredder.location.GoogleLocationProvider;
import com.shredder.location.LocationAccuracy;
import com.shredder.mqtt.QualityOfService;

public class ServiceEventBusRegistry {

    private final String uniqueId;
    private final LocationSyncPluginController locationSync;
    private final OnServiceIntentBroadcastReceiver broadcastReceiver;
    private final Context context;

    public ServiceEventBusRegistry(Context applicationContext) {
        context = applicationContext;
        uniqueId = UniqueId.createUniqueDeviceId(applicationContext);
        GoogleLocationProvider googleLocationProvider = new GoogleLocationProvider(applicationContext);

        LocationSync.Configuration config = new LocationSync.Configuration() {
            @Override
            public LocationAccuracy getAccuracy() {
                return LocationAccuracy.High;
            }

            @Override
            public String getHost() {
                return "tcp://52.28.76.170:1883";
            }

            @Override
            public QualityOfService getQualityOfService() {
                return QualityOfService.FireAndForget;
            }

            @Override
            public String getUniqueId() {
                return uniqueId;
            }

            @Override
            public String getUser() {
                return "garry";
            }

            @Override
            public String getPassword() {
                return "i8the4kin$";
            }
        };

        locationSync = new LocationSyncPluginController(googleLocationProvider, config);
        locationSync.setCallback(new LocationSyncPluginController.Listener() {
            @Override
            public void onUserUpdateReceived(UserUpdate update) {
                context.sendBroadcast(IntentBuilder.createUpdateGroupIntent(update));
            }
        });
        broadcastReceiver = new OnServiceIntentBroadcastReceiver(locationSync);
        applicationContext.registerReceiver(broadcastReceiver, broadcastReceiver.getIntentFilter());
    }

    public void start(String group, String name) {
        locationSync.setName(name);
        locationSync.setGroup(group);
        locationSync.start();
    }

    public void stop() {
        context.unregisterReceiver(broadcastReceiver);
    }
}