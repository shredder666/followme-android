package com.shredder.followme.service.manager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.shredder.followme.MainActivity;
import com.shredder.followme.R;
import com.shredder.followme.builder.IntentBuilder;

public class FollowMeNotificationHandler {

    public static final int ONGOING_NOTIFICATION_ID = 667;
    private PendingIntent pendingIntentStop;
    private PendingIntent pendingIntentDelete;
    private PendingIntent pendingIntentOpenApp;

    private NotificationCompat.Builder mBuilder;

    private final NotificationManagerCompat mNotificationManager;
    private String notificationText;

    public FollowMeNotificationHandler(Context context) {
        mNotificationManager = NotificationManagerCompat.from(context);
        notificationText = context.getString(R.string.notification_text);
        mBuilder = createNotification(context);
    }

    private void createStopIntent(Context context) {
        Intent stopIntent = IntentBuilder.createStopIntent();
        pendingIntentStop = PendingIntent.getBroadcast(context, 0, stopIntent, 0);
    }

    private void createOpenAppIntent(Context context) {
        Intent resultIntent = new Intent(context, MainActivity.class);
        pendingIntentOpenApp = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void createDismissIntent(Context context) {
        Intent deleteIntent = IntentBuilder.createStopIntent();
        pendingIntentDelete = PendingIntent.getService(context, 0, deleteIntent, 0);
    }

    public Notification showOngoingNotification() {
        updateNotification(ONGOING_NOTIFICATION_ID);
        return mBuilder.build();
    }

    public void cancelOngoingNotification() {
        mNotificationManager.cancel(ONGOING_NOTIFICATION_ID);
    }

    public void updateNotification(int id) {
        mBuilder.setContentText(notificationText);
        mNotificationManager.notify(id, mBuilder.build());
    }

    private NotificationCompat.Builder createNotification(Context context) {
        createStopIntent(context);
        createOpenAppIntent(context);
        createDismissIntent(context);

        return new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_location_on_white_24dp)
                .addAction(R.drawable.ic_clear_grey600_36dp, context.getString(R.string.notification_stop), pendingIntentStop)
                .setContentTitle(context.getString(R.string.notification_title))
                .setDeleteIntent(pendingIntentDelete)
                .setContentIntent(pendingIntentOpenApp);
    }
}
