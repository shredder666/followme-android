package com.shredder.followme.service.plugincontroller;


import com.shredder.followme.model.UserUpdate;
import com.shredder.followme.service.manager.LocationSync;
import com.shredder.location.GoogleLocationProvider;

public class LocationSyncPluginController {
    private final LocationSync locationSync;
    private Listener listener;

    public LocationSyncPluginController(GoogleLocationProvider locationProvider, LocationSync.Configuration config) {
        locationSync = new LocationSync(config, locationProvider);
        LocationSync.Listener locationListener = new LocationSync.Listener() {
            @Override
            public void onMessageReceived(UserUpdate userUpdate) {
                if (listener != null) {
                    listener.onUserUpdateReceived(userUpdate);
                }
            }
        };
        locationSync.setOnLocationReceivedListener(locationListener);
    }

    public void setName(String name) {
        locationSync.setName(name);
    }

    public void setGroup(String group) {
        locationSync.setGroup(group);
    }

    public void setCallback(Listener listener) {
        this.listener = listener;
    }

    public void start(){
        locationSync.start();
    }

    public void stop(){
        locationSync.stop();
    }

    public interface Listener {
        void onUserUpdateReceived(UserUpdate update);
    }
}