package com.shredder.followme.plugincontroller;


import com.shredder.common.eventbus.event.ActivityLifeCycleEvent;
import com.shredder.common.eventbus.plugincontroller.PluginController;
import com.shredder.followme.event.show.ShowMapEvent;
import com.shredder.followme.event.show.ShowNavigationEvent;

import org.greenrobot.eventbus.Subscribe;

public class ApplicationStartupPluginController extends PluginController {

    @Subscribe
    public void onActivityLifeCycle(ActivityLifeCycleEvent event) {
        if (event.isOnPostCreate()) {
            post(new ShowMapEvent());
            post(new ShowNavigationEvent());
        }
    }
}
