package com.shredder.followme.plugincontroller.show;

import com.shredder.common.eventbus.plugincontroller.SingleFragmentPluginController;
import com.shredder.followme.event.show.ShowNavigationEvent;
import com.shredder.followme.plugin.navigation.NavigationFragment;

import org.greenrobot.eventbus.Subscribe;

public class ShowNavigationPluginController extends SingleFragmentPluginController<ShowNavigationEvent, NavigationFragment> {

    public ShowNavigationPluginController() {
        super(NavigationFragment.class);
    }

    @Subscribe
    public void onShow(ShowNavigationEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected NavigationFragment createFragment(ShowNavigationEvent showNavigationEvent) {
        return NavigationFragment.newInstance();
    }
}
