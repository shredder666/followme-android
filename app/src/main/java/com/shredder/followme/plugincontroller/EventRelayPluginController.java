package com.shredder.followme.plugincontroller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.shredder.common.eventbus.plugincontroller.PluginController;
import com.shredder.followme.builder.IntentBuilder;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ConfigNameEvent;
import com.shredder.followme.event.ControlPlayEvent;
import com.shredder.followme.event.ControlStopEvent;
import com.shredder.followme.event.IntentReceivedEvent;
import com.shredder.followme.event.ServiceConnectedRequestEvent;
import com.shredder.followme.event.ServiceConnectedResponseEvent;
import com.shredder.followme.event.UserUpdateEvent;
import com.shredder.followme.model.UserUpdate;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class EventRelayPluginController extends PluginController {
    private final static String TAG = "Activity Relay";
    private final Context context;
    private String name;
    private String group;

    public EventRelayPluginController(Context context) {
        this.context = context;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigNameEvent event) {
        name = event.getName();
        context.sendBroadcast(IntentBuilder.createConfigNameIntent(event.getName()));
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigGroupEvent event) {
        Log.i(TAG, "onSettings(ConfigGroupEvent event)");
        group = event.getGroup();
        context.sendBroadcast(IntentBuilder.createConfigGroupIntent(event.getGroup()));
    }

    @Subscribe
    public void onSettings(ControlPlayEvent event) {
        context.startService(IntentBuilder.createStartServiceIntent(context, group, name));
    }

    @Subscribe
    public void onSettings(ControlStopEvent event) {
        context.stopService(IntentBuilder.createStopServiceIntent(context));
    }
    @Subscribe
    public void onSettings(ServiceConnectedRequestEvent event) {
        context.sendBroadcast(IntentBuilder.createPingRequestIntent());
    }

    @Subscribe
    public void onEvent(IntentReceivedEvent event) {
        Intent intent = event.getIntent();
        String action = intent.getAction();
        switch (action) {
            case IntentBuilder.ACTION_UPDATE:
                UserUpdate userUpdate = (UserUpdate) intent.getSerializableExtra(IntentBuilder.EXTRA_USER_UPDATE);
                post(new UserUpdateEvent(userUpdate));
                break;
            case IntentBuilder.ACTION_STOP:
                post(new ControlStopEvent());
                break;
            case IntentBuilder.ACTION_PING:
                if (IntentBuilder.isPingResponse(intent)) {
                    post(new ServiceConnectedResponseEvent());
                }
                break;
            default:
                Log.e(TAG, "Unknown intent found    : " + action);
                Log.e(TAG, "Unknown intent should be: " + IntentBuilder.ACTION_UPDATE);
                break;
        }
    }
}
