package com.shredder.followme.plugincontroller;

import android.location.Location;

import com.shredder.common.eventbus.plugincontroller.PluginController;
import com.shredder.followme.event.PermissionsUpdatedEvent;
import com.shredder.followme.event.sticky.LocationUpdatedEvent;
import com.shredder.location.GoogleLocationProvider;
import com.shredder.location.LocationAccuracy;
import com.shredder.location.LocationConfig;

import org.greenrobot.eventbus.Subscribe;

public class OwnLocationPluginController extends PluginController {
    private final GoogleLocationProvider locationProvider;
    private final GoogleLocationProvider.OnLocationChangedListener locationListener = new GoogleLocationProvider.OnLocationChangedListener() {
        @Override
        public void onLocationChanged(Location result) {
            postSticky(new LocationUpdatedEvent(result));
        }
    };

    public OwnLocationPluginController(GoogleLocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    @Subscribe(sticky = true)
    public void onEvent(PermissionsUpdatedEvent event) {
        if (!event.isGranted()) {
            locationProvider.removeLocationListener();
        }
        locationProvider.setConfig(new LocationConfig() {
            @Override
            public LocationAccuracy getAccuracy() {
                return LocationAccuracy.Highest;
            }
        });
        locationProvider.addLocationListener(locationListener);
    }
}
