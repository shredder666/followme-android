package com.shredder.followme.plugincontroller.show;

import com.shredder.common.eventbus.plugincontroller.SingleFragmentPluginController;
import com.shredder.followme.event.show.ShowSettingsEvent;
import com.shredder.followme.plugin.settings.SettingsFragment;

import org.greenrobot.eventbus.Subscribe;

public class ShowSettingsPluginController extends SingleFragmentPluginController<ShowSettingsEvent, SettingsFragment> {

    public ShowSettingsPluginController() {
        super(SettingsFragment.class);
    }

    @Subscribe
    public void onShow(ShowSettingsEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected SettingsFragment createFragment(ShowSettingsEvent showSettingsEvent) {
        return SettingsFragment.newInstance();
    }

    @Override
    protected String createActionBarTitle() {
        return "Profile";
    }
}
