package com.shredder.followme.plugincontroller.show;


import com.shredder.common.eventbus.plugincontroller.SingleFragmentPluginController;
import com.shredder.followme.FollowMeApplication;
import com.shredder.followme.R;
import com.shredder.followme.event.show.ShowMapEvent;
import com.shredder.followme.plugin.map.FmMapFragment;

import org.greenrobot.eventbus.Subscribe;

public class ShowMapPluginController extends SingleFragmentPluginController<ShowMapEvent, FmMapFragment> {
    public ShowMapPluginController() {
        super(FmMapFragment.class);
    }

    @Subscribe
    public void onShow(ShowMapEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected boolean shouldDisplayFragment(ShowMapEvent event) {
        return true;
    }

    @Override
    protected String createActionBarTitle() {
        return FollowMeApplication.getStringResource(R.string.app_name);
    }

    @Override
    protected FmMapFragment createFragment(ShowMapEvent showMapEvent) {
        return FmMapFragment.newInstance();
    }
}
