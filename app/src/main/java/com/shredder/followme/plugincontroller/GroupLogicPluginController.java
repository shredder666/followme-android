package com.shredder.followme.plugincontroller;

import com.shredder.common.eventbus.plugincontroller.PluginController;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ControlStopEvent;
import com.shredder.followme.event.NewUserJoinedGroupEvent;
import com.shredder.followme.event.UserUpdateEvent;
import com.shredder.followme.model.UserUpdate;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

public class GroupLogicPluginController extends PluginController {

    private final HashMap<String, UserUpdate> groupRecords = new HashMap<>();

    public GroupLogicPluginController() {

    }

    @Subscribe
    public void onEvent(UserUpdateEvent event) {
        UserUpdate userUpdate = event.getUserUpdate();
        String uniqueId = userUpdate.getUniqueId();

        if (!groupRecords.containsKey(uniqueId)) {
            post(new NewUserJoinedGroupEvent(userUpdate));
        }

        groupRecords.put(uniqueId, userUpdate);
    }


    @Subscribe
    public void onEvent(ControlStopEvent event) {
        clearAll();
    }

    @Subscribe
    public void onEvent(ConfigGroupEvent event) {
        clearAll();
    }

    private void clearAll() {
        groupRecords.clear();
    }
}
