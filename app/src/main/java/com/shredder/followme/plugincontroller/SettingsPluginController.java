package com.shredder.followme.plugincontroller;

import com.shredder.common.eventbus.event.ActivityLifeCycleEvent;
import com.shredder.common.eventbus.plugincontroller.PluginController;
import com.shredder.followme.event.ConfigGroupEvent;
import com.shredder.followme.event.ConfigNameEvent;
import com.shredder.followme.model.UserPreferences;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SettingsPluginController extends PluginController {
    private final UserPreferences preferences;

    public SettingsPluginController(UserPreferences preferences) {
        this.preferences = preferences;
    }

    @Subscribe
    public void onLifeCycle(ActivityLifeCycleEvent event) {
        if (event.isOnPostCreate()) {

            String name = preferences.getName();
            String group = preferences.getGroup();

            postSticky(new ConfigGroupEvent(group));
            postSticky(new ConfigNameEvent(name));

//            if(StringUtils.isEmpty(name)){
//                post(new ShowSettingsEvent());
//            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigNameEvent event) {
        preferences.setName(event.getName());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSettings(ConfigGroupEvent event) {
        preferences.setGroup(event.getGroup());
    }
}
