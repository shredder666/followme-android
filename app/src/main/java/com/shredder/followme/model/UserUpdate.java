package com.shredder.followme.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserUpdate implements Serializable{
    private final String userName;
    private final String uniqueId;
    private final String group;
    private final double latitude;
    private final double longitude;
    private final float accuracy;
    private final float bearing;
}
