package com.shredder.followme.model;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.shredder.common.persistence.SharedPreferenceBase;
import com.shredder.followme.FollowMeApplication;

public class MapPreferences extends SharedPreferenceBase {
    public static final String MAP_PREFERENCES = MapPreferences.class.getSimpleName();

    public static final float ZOOM_LEVEL_DEFAULT = 5.0f;
    public static final double INITIAL_CAMERA_POSITION_LATITUDE = 48.1351253;
    public static final double INITIAL_CAMERA_POSITION_LONGITUDE = 11.5819806;

    private static final LatLng INITIAL_CAMERA_POSITION = new LatLng(INITIAL_CAMERA_POSITION_LATITUDE, INITIAL_CAMERA_POSITION_LONGITUDE);

    public static final String PREF_LOCATION_SET = MAP_PREFERENCES + "_LOCATION_SET";
    public static final String PREF_LOCATION_LATITUDE = MAP_PREFERENCES + "_LOCATION_LATITUDE";
    public static final String PREF_LOCATION_LONGITUDE = MAP_PREFERENCES + "_LOCATION_LONGITUDE";

    public static final String PREF_BOUNDS_SET = MAP_PREFERENCES + "_BOUNDS_SET";
    public static final String PREF_BOUNDS_SW_LATITUDE = MAP_PREFERENCES + "_BOUNDS_SW_LATITUDE";
    public static final String PREF_BOUNDS_SW_LONGITUDE = MAP_PREFERENCES + "_BOUNDS_SW_LONGITUDE";
    public static final String PREF_BOUNDS_NE_LATITUDE = MAP_PREFERENCES + "_BOUNDS_NE_LATITUDE";
    public static final String PREF_BOUNDS_NE_LONGITUDE = MAP_PREFERENCES + "_BOUNDS_NE_LONGITUDE";
    private static final String PREF_ZOOM = "_ZOOM";

    private static MapPreferences sInstance;

    public static MapPreferences getInstance() {
        if (sInstance == null) {
            sInstance = new MapPreferences(FollowMeApplication.getInstance());
        }
        return sInstance;
    }

    @Override
    protected String getFileName() {
        return MAP_PREFERENCES;
    }

    public MapPreferences(Context context) {
        super(context);
    }

    public void setLastLocation(LatLng location) {
        setFloat(PREF_LOCATION_LATITUDE, (float) location.latitude);
        setFloat(PREF_LOCATION_LONGITUDE, (float) location.longitude);
        setBoolean(PREF_LOCATION_SET, true);
    }

    public LatLng getLastLocation() {
        LatLng lastLocation = new LatLng(
                getFloat(PREF_LOCATION_LATITUDE, (float)INITIAL_CAMERA_POSITION_LATITUDE),
                getFloat(PREF_LOCATION_LONGITUDE, (float)INITIAL_CAMERA_POSITION_LONGITUDE));
        return lastLocation;
    }

    public boolean hasLastLocation() {
        return getBoolean(PREF_LOCATION_SET, false);
    }

    public void setLastBounds(LatLngBounds bounds) {
        setFloat(PREF_BOUNDS_SW_LATITUDE, (float) bounds.southwest.latitude);
        setFloat(PREF_BOUNDS_SW_LONGITUDE, (float) bounds.southwest.longitude);
        setFloat(PREF_BOUNDS_NE_LATITUDE, (float) bounds.northeast.latitude);
        setFloat(PREF_BOUNDS_NE_LONGITUDE, (float) bounds.northeast.longitude);
        setBoolean(PREF_BOUNDS_SET, true);
    }

    public LatLngBounds getLastBounds() {
        LatLng sw = new LatLng(
                getFloat(PREF_BOUNDS_SW_LATITUDE, 5),
                getFloat(PREF_BOUNDS_SW_LONGITUDE, 5));
        LatLng ne = new LatLng(
                getFloat(PREF_BOUNDS_NE_LATITUDE, 7),
                getFloat(PREF_BOUNDS_NE_LONGITUDE, 7));
        LatLngBounds lastBounds = new LatLngBounds(sw, ne);
        return lastBounds;
    }

    public boolean hasLastBounds() {
        return getBoolean(PREF_BOUNDS_SET, false);
    }

    public void setLastZoomLevel(float zoom) {
        setFloat(PREF_ZOOM, zoom);
    }

    public float getLastZoomLevel(){
        return getFloat(PREF_ZOOM, ZOOM_LEVEL_DEFAULT);
    }
}
