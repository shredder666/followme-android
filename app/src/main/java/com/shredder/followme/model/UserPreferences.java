package com.shredder.followme.model;

import android.content.Context;

import com.shredder.common.persistence.SharedPreferenceBase;


public class UserPreferences extends SharedPreferenceBase {

    public static final String GROUP = "group";
    public static final String NAME = "name";
    public static final String TIME = "time";

    public UserPreferences(Context context) {
        super(context);
    }

    @Override
    protected String getFileName() {
        return UserPreferences.class.getSimpleName();
    }

    public void setName(String name) {
        setString(NAME, name);
    }

    public String getName() {
        return getString(NAME, "");
    }

    public void setGroup(String group) {
        setString(GROUP, group);
    }

    public String getGroup() {
        return getString(GROUP, "");
    }

    public void setTime(long time) {
        setLong(TIME, time);
    }

    public long getTime() {
        return getLong(TIME, 0);
    }

}
