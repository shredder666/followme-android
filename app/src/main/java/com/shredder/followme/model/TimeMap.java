package com.shredder.followme.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lombok.Data;

@Data
public class TimeMap {
    public static final long FIVE_SECONDS = 5 * 1000;
    public static final long ONE_MINUTE = 60 * 1000;
    public static final long FIVE_MINUTES = 5 * ONE_MINUTE;
    public static final long TEN_MINUTES = 10 * ONE_MINUTE;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    public static final long FIFTEEN_MINUTES = 15 * ONE_MINUTE;
    public static final long THREE_HOURS = 3 * ONE_HOUR;
    public static final long ONE_DAY = 24 * ONE_HOUR;

    public static final long ONE_WEEK = 7 * ONE_DAY;
    public static final long FOREVER = -1;
    public static final String DATE_FORMAT = "hh:mm EEEE d MMMM";
    public static final int TWO_SECONDS = 2000;

    private final String display;
    private final long timeInMilliseconds;

    public TimeMap(String display, long timeInMilliseconds) {
        this.display = display;
        this.timeInMilliseconds = timeInMilliseconds;
    }

    @Override
    public String toString() {
        return display;
    }

    public static List<TimeMap> createDefaultOptions() {
        List<TimeMap> options = new ArrayList<>();
        options.add(new TimeMap("No change", 0));
        options.add(new TimeMap("One Minute", ONE_MINUTE));
        options.add(new TimeMap("Five Minutes", FIVE_MINUTES));
        options.add(new TimeMap("Fifteen Minutes", FIFTEEN_MINUTES));
        options.add(new TimeMap("One Hour", ONE_HOUR));
        options.add(new TimeMap("Three Hours", 3 * ONE_HOUR));
        options.add(new TimeMap("One Day", ONE_DAY));
        options.add(new TimeMap("One Week", ONE_WEEK));
        options.add(new TimeMap("Forever", FOREVER));
        return options;
    }

    public static String getDateString(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static boolean isValid(long time) {
        return time != 0;
    }

    public static boolean isExpired(long time) {
        if (time == TimeMap.FOREVER) {
            return false;
        }
        return time < System.currentTimeMillis();
    }
}
