package com.shredder.followme.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MqttPayload {
    private final String userName;
    private final double lat;
    private final double lon;
    private final float accuracy;
    private final float bearing;
}
