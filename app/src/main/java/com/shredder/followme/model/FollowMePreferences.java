package com.shredder.followme.model;

import android.content.Context;

import com.shredder.common.persistence.SharedPreferenceBase;

public class FollowMePreferences extends SharedPreferenceBase {
    private static final String HAS_SHOWN_FIT_HINT = "HAS_SHOWN_FIT_HINT";

    public FollowMePreferences(Context context) {
        super(context);
    }

    @Override
    protected String getFileName() {
        return "prefs";
    }

    public boolean hasShownFitButtonHint() {
        return getBoolean(HAS_SHOWN_FIT_HINT, false);
    }

    public void setHasShownFitButtonHint() {
        setBoolean(HAS_SHOWN_FIT_HINT, true);
    }
}
