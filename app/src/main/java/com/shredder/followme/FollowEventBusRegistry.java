package com.shredder.followme;

import android.content.Context;

import com.shredder.common.eventbus.ShEventBusRegistry;
import com.shredder.common.eventbus.plugincontroller.EventBusLoggingPluginController;
import com.shredder.followme.model.UserPreferences;
import com.shredder.followme.plugincontroller.ApplicationStartupPluginController;
import com.shredder.followme.plugincontroller.EventRelayPluginController;
import com.shredder.followme.plugincontroller.GroupLogicPluginController;
import com.shredder.followme.plugincontroller.OwnLocationPluginController;
import com.shredder.followme.plugincontroller.SettingsPluginController;
import com.shredder.followme.plugincontroller.show.ShowMapPluginController;
import com.shredder.followme.plugincontroller.show.ShowNavigationPluginController;
import com.shredder.followme.plugincontroller.show.ShowSettingsPluginController;
import com.shredder.location.GoogleLocationProvider;

import java.util.Arrays;
import java.util.List;

public class FollowEventBusRegistry extends ShEventBusRegistry {

    public FollowEventBusRegistry(Context applicationContext) {
        super(applicationContext);
    }

    @Override
    protected void onBeforeRegisterDefaultSubscribers() {
    }

    @Override
    protected void onBeforeUnregisterAllEventSubscribers() {

    }

    @Override
    protected List<ShEventBusRegistry.EventBusSubscriber> createDefaultSubscribers() {
        return Arrays.asList(new ShEventBusRegistry.EventBusSubscriber[]{
                        new ApplicationStartupPluginController(),
                        new SettingsPluginController(new UserPreferences(applicationContext)),
                        new ShowMapPluginController(),
                        new ShowSettingsPluginController(),
                        new ShowNavigationPluginController(),
                        new GroupLogicPluginController(),
                        new OwnLocationPluginController(new GoogleLocationProvider(applicationContext)),
                        new EventRelayPluginController(applicationContext),
                        new EventBusLoggingPluginController()
                }
        );
    }
}
