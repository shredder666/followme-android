package com.shredder.common.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shredder.followme.R;

import java.util.ArrayList;
import java.util.List;

public class ShredderAdapter extends RecyclerView.Adapter<DefaultListItemViewHolder> {

    private List<DefaultListItem> mDataset = new ArrayList<>();

    @Override
    public DefaultListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.default_list_item, parent, false);
        return new DefaultListItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DefaultListItemViewHolder holder, int position) {
        holder.bind(mDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void add(int position, DefaultListItem item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public void setItems(List<DefaultListItem> items) {
        mDataset.clear();
        mDataset.addAll(items);
        notifyDataSetChanged();
    }
}
