package com.shredder.common.recycler;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shredder.common.eventbus.ShEventBus;
import com.shredder.followme.R;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DefaultListItemViewHolder extends RecyclerView.ViewHolder {

    private final Resources resources;
    @BindView(R.id.default_container) View container;
    @BindView(R.id.default_image) ImageView imageIcon;
    @BindView(R.id.default_title) TextView textViewTitle;
    @BindView(R.id.default_subtitle) TextView textViewSubTitle;

    public DefaultListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        resources = itemView.getResources();
    }

    public void bind(final DefaultListItem item) {
        image(item);
        title(item);
        subtitle(item);
        selectedEvent(item);
    }

    private void title(DefaultListItem item) {
        textViewTitle.setText(item.getTitle());
    }

    private void subtitle(DefaultListItem item) {
        textViewSubTitle.setText(item.getSubtitle());
        textViewSubTitle.setVisibility(StringUtils.isEmpty(item.getSubtitle()) ? View.GONE : View.VISIBLE);
    }

    private void image(DefaultListItem item) {
        Drawable drawable;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable = resources.getDrawable(item.getImageResId(), null);
        } else {
            drawable = resources.getDrawable(item.getImageResId());
        }

        imageIcon.setImageDrawable(drawable);
    }

    private void selectedEvent(final DefaultListItem item) {
        if (item.getSelectedEvent() == null) {
            container.setOnClickListener(null);
            container.setClickable(false);
            container.setFocusable(false);
        } else {
            container.setClickable(true);
            container.setFocusable(true);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShEventBus.getInstance().post(item.getSelectedEvent());
                }
            });
        }
    }
}
