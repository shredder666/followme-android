package com.shredder.common.recycler;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDivider;
    private int mInsets;

    public DividerDecoration(Drawable divider) {
        mDivider = divider;
        mInsets = 0;
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        drawVertical(canvas, parent);
    }

    public void drawVertical(Canvas canvas, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            if (shouldDrawDivider(parent, child)) {
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin + mInsets ;
                final int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(canvas);
            }

        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(mInsets, mInsets, mInsets, mInsets + mDivider.getIntrinsicHeight());
    }

    protected boolean shouldDrawDivider(RecyclerView recyclerView, View item) {
        return true;
    }

    public final boolean isFirstItem(RecyclerView recyclerView, View item) {
        return recyclerView.getChildAdapterPosition(item) == 0;
    }

    public final boolean isLastItemItem(RecyclerView recyclerView, View item) {
        return recyclerView.getChildAdapterPosition(item) == recyclerView.getAdapter().getItemCount() -1;
    }
}

