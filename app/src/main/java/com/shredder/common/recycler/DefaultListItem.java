package com.shredder.common.recycler;

import android.support.annotation.DrawableRes;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DefaultListItem {
    @DrawableRes
    private final int imageResId;
    private final String title;
    private final String subtitle;
    private Object selectedEvent;
}
