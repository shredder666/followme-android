package com.shredder.common.eventbus.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * NOTE: The eventbus based architecture only supports one instance of EventReportingActivity so there should also exist one
 * extension of SxEventReportingActivity per application.
 */
public abstract class EventReportingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LifeCycleEventReporter.reportOnCreate(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        LifeCycleEventReporter.reportOnPostCreate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LifeCycleEventReporter.reportOnStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LifeCycleEventReporter.reportOnResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LifeCycleEventReporter.reportOnPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LifeCycleEventReporter.reportOnStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LifeCycleEventReporter.reportOnDestroy(this);
    }
}
