package com.shredder.common.eventbus.event;

import android.support.v4.app.Fragment;

import lombok.Getter;

@Getter
public class FragmentLifeCycleEvent {
    private Class<? extends Fragment> fragmentClass;
    private boolean onCreate = false;
    private boolean onStart = false;
    private boolean onAttach = false;
    private boolean onResume = false;
    private boolean onPause = false;
    private boolean onDetach = false;
    private boolean onStop = false;
    private boolean onDestroy = false;

    public static FragmentLifeCycleEvent onCreate(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onCreate = true;
        return event;
    }

    public static FragmentLifeCycleEvent onStart(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onStart = true;
        return event;
    }

    public static FragmentLifeCycleEvent onAttach(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onAttach = true;
        return event;
    }

    public static FragmentLifeCycleEvent onResume(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onResume = true;
        return event;
    }

    public static FragmentLifeCycleEvent onPause(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onPause = true;
        return event;
    }

    public static FragmentLifeCycleEvent onDetach(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onDetach = true;
        return event;
    }

    public static FragmentLifeCycleEvent onStop(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onStop = true;
        return event;
    }

    public static FragmentLifeCycleEvent onDestroy(Class<? extends Fragment> fragmentClass) {
        FragmentLifeCycleEvent event = new FragmentLifeCycleEvent();
        event.fragmentClass = fragmentClass;
        event.onDestroy = true;
        return event;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(fragmentClass.getSimpleName());
        sb.append(".");
        if (onCreate) {
            sb.append("onCreate()");
        } else if (onStart) {
            sb.append("onStart()");
        } else if (onResume) {
            sb.append("onResume()");
        } else if (onAttach) {
            sb.append("onAttach()");
        } else if (onDetach) {
            sb.append("onDetach()");
        } else if (onPause) {
            sb.append("onPause()");
        } else if (onStop) {
            sb.append("onStop()");
        } else if (onDestroy) {
            sb.append("onDestroy()");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
