package com.shredder.common.eventbus.plugincontroller;

import android.util.Log;

import org.greenrobot.eventbus.Subscribe;

public class EventBusLoggingPluginController extends PluginController {

    private static final String TAG = "EventBus";

    @Subscribe
    public void onLog(Object event) {
        Log.d(TAG, event.toString());
    }
}
