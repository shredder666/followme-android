package com.shredder.common.eventbus.presenter;

import com.shredder.common.eventbus.ShEventBus;
import com.shredder.common.eventbus.ShEventBusRegistry;

public abstract class EventListeningPresenter extends EventPostingPresenter {

    public void startEventListening() {
        registerForEvents();
    }

    public void stopEventListening() {
        ShEventBusRegistry.getInstance().unregisterSubscriber(this);
    }

    private void registerForEvents() {
        ShEventBusRegistry.getInstance().registerSubscriber(new ShEventBusRegistry.EventBusSubscriber() {
            @Override
            public Object register(ShEventBus eventBus) {
                eventBus.register(EventListeningPresenter.this);
                return EventListeningPresenter.this;
            }

            @Override
            public void unregister(ShEventBus eventBus) {
                eventBus.unregister(EventListeningPresenter.this);
            }
        });
    }
}
