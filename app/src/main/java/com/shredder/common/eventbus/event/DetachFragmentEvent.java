package com.shredder.common.eventbus.event;

import android.support.v4.app.Fragment;

public class DetachFragmentEvent extends TransactionFragmentEvent {

    public DetachFragmentEvent(Class<? extends Fragment> fragmentClass) {
        super(fragmentClass);
    }
}
