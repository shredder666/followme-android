package com.shredder.common.eventbus.event;


import android.support.v4.app.Fragment;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BringFragmentToFrontEvent extends TransactionFragmentEvent {

    private final String actionBarTitle;

    public BringFragmentToFrontEvent(Class<? extends Fragment> fragmentClass, String actionBarTitle) {
        super(fragmentClass);
        this.actionBarTitle = actionBarTitle;
    }
}