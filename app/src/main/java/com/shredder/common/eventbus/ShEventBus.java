package com.shredder.common.eventbus;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ShEventBus extends EventBus {
    private static volatile ShEventBus defaultInstance;
    private final ScheduledExecutorService mExecutorService;

    private ShEventBus() {
        super();
        mExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public ScheduledFuture<Object> postDelayed(Object event, long delay) {
        return mExecutorService.schedule(new PostEventCallable(this, event), delay, TimeUnit.MILLISECONDS);
    }

    private class PostEventCallable implements Callable<Object> {
        private final ShEventBus mEventBus;
        private final Object mEvent;

        public PostEventCallable(ShEventBus eventBus, Object event) {
            mEventBus = eventBus;
            mEvent = event;
        }

        @Override
        public Object call() throws Exception {
            mEventBus.post(mEvent);
            return null;
        }
    }

    public static ShEventBus getInstance() {
        if (defaultInstance == null) {
            synchronized (ShEventBus.class) {
                if (defaultInstance == null) {
                    defaultInstance = new ShEventBus();
                }
            }
        }
        return defaultInstance;
    }

    public static void postEvent(Object event) {
        getInstance().post(event);
    }

    public static void postEventDelayed(Object event, long delay) {
        getInstance().postDelayed(event, delay);
    }

    public static void postStickyEvent(Object event) {
        getInstance().postSticky(event);
    }

    public static <T> T getSticky(Class<T> eventType) {
        return getInstance().getStickyEvent(eventType);
    }

    public static <T> T removeSticky(Class<T> eventType) {
        return getInstance().removeStickyEvent(eventType);
    }
}
