package com.shredder.common.eventbus.ui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.shredder.common.eventbus.BackStackUtils;
import com.shredder.common.eventbus.ShEventBus;
import com.shredder.common.eventbus.ShEventBusRegistry;
import com.shredder.common.eventbus.event.AttachFragmentEvent;
import com.shredder.common.eventbus.event.BringFragmentToFrontEvent;
import com.shredder.common.eventbus.event.ClearBackStackFragmentsEvent;
import com.shredder.common.eventbus.event.DetachFragmentEvent;
import com.shredder.common.eventbus.event.TransactionFragmentEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * NOTE: Since the eventbus based architecture only supports one instance of EventReportingActivity there should also exist one
 * extension of SxEventBasedFragmentActivity per application.
 */
public abstract class EventBasedFragmentActivity extends EventReportingActivity implements ShEventBusRegistry.EventBusSubscriber {

    protected String mEmptyBackStackActionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                updateActionBar();
            }
        });

        ShEventBusRegistry.getInstance().registerSubscriber(this);
    }

    @Override
    protected void onDestroy() {
        ShEventBusRegistry.getInstance().unregisterSubscriber(this);
        super.onDestroy();
    }

    @Override
    public Object register(ShEventBus eventBus) {
        eventBus.register(this);
        return this;
    }

    @Override
    public void unregister(ShEventBus eventBus) {
        eventBus.unregister(this);
    }

    @SuppressWarnings("UnusedDeclaration")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void attach(AttachFragmentEvent event) {
        Fragment fragment = event.getFragment();
        if (fragment instanceof DialogFragment) {
            openDialog((DialogFragment) fragment);
        } else {
            attachFragment(event);
        }
    }

    private void openDialog(DialogFragment dialog) {
        dialog.show(getSupportFragmentManager(), ((Object) dialog).getClass().getSimpleName());
    }

    private void attachFragment(AttachFragmentEvent event) {
        int containerId = getContainerId(event);
        if (!event.isAddToBackStack()) {
            clearBackStackFragments();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (event.isAddToBackStack()) {
            transaction.addToBackStack(event.getFragmentClass().getName());
            transaction.setBreadCrumbTitle(event.getActionBarTitle());
        } else if (event.getActionBarTitle() != null) {
            mEmptyBackStackActionBarTitle = event.getActionBarTitle();
            getSupportActionBar().setTitle(event.getActionBarTitle());
        }
        transaction.setCustomAnimations(event.getEnterAnimation(), event.getExitAnimation(), event.getPopEnterAnimation(), event.getPopExitAnimation());
        transaction.replace(containerId, event.getFragment(), event.getFragmentClass().getName());
        transaction.commit();
    }

    @SuppressWarnings("UnusedDeclaration")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void detach(DetachFragmentEvent event) {
        if (!isFragmentAttached(event)) {
            return;
        }

        Fragment fragment = getFragment(event);
        String tag = getFragmentTag(event);
        removeFragment(fragment, tag);
    }

    @SuppressWarnings("UnusedDeclaration")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void bringFragmentToFront(BringFragmentToFrontEvent event) {
        BackStackUtils.bringFragmentToFront(getSupportFragmentManager(), event.getFragmentClass().getName(), getContainerId(event));
    }

    @SuppressWarnings("UnusedDeclaration")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void clearBackStackFragments(ClearBackStackFragmentsEvent event) {
        clearBackStackFragments();
    }

    protected abstract int getContainerId(TransactionFragmentEvent event);

    private void clearBackStackFragments() {
        FragmentManager fm = getSupportFragmentManager();
        while (fm.getBackStackEntryCount() != 0) {
            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(0);
            Fragment fragment = fm.findFragmentByTag(entry.getName());
            if (fragment != null) {
                removeFragment(fragment, fragment.getTag());
            }
        }
    }

    private Fragment getFragment(DetachFragmentEvent event) {
        String tag = getFragmentTag(event);
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    private String getFragmentTag(DetachFragmentEvent event) {
        return event.getFragmentClass().getName();
    }

    private boolean isFragmentAttached(DetachFragmentEvent event) {
        Fragment fragment = getFragment(event);
        return fragment != null;
    }

    private void removeFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected boolean isBackStackEmpty() {
        FragmentManager fm = getSupportFragmentManager();
        return fm.getBackStackEntryCount() == 0;
    }

    protected void updateActionBar() {
        if (isBackStackEmpty()) {
            getSupportActionBar().setTitle(mEmptyBackStackActionBarTitle);
        } else {
            getSupportActionBar().setTitle(BackStackUtils.getBackStackCurrentItemTitle(getSupportFragmentManager()));
        }
        getSupportActionBar().invalidateOptionsMenu();
    }
}