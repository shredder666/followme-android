package com.shredder.common.eventbus.presenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PresenterHierarchy<View> {

    private final List<ChildPresenter> childPresenters = new ArrayList<>();

    public PresenterHierarchy(ChildPresenter... childPresenter) {
        this.childPresenters.addAll(Arrays.asList(childPresenter));
    }

    public void addChildPresenter(ChildPresenter childPresenter) {
        childPresenters.add(childPresenter);
    }

    public void start(View view) {
        for (ChildPresenter<View> childPresenter : childPresenters) {
            childPresenter.start(view);
        }
    }

    public void stop() {
        for (ChildPresenter childPresenter: childPresenters) {
            childPresenter.stop();
        }
    }
}
