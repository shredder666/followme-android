package com.shredder.common.eventbus.event;

import android.app.Activity;

import lombok.Getter;

@Getter
public class ActivityLifeCycleEvent {
    private final Class<? extends Activity> activityClass;
    private boolean onCreate = false;
    private boolean onPostCreate = false;
    private boolean onStart = false;
    private boolean onResume = false;
    private boolean onPause = false;
    private boolean onStop = false;
    private boolean onDestroy = false;

    public ActivityLifeCycleEvent(Class<? extends Activity> activityClass) {
        this.activityClass = activityClass;
    }

    public static ActivityLifeCycleEvent onCreate(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onCreate = true;
        return event;
    }

    public static ActivityLifeCycleEvent onPostCreate(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onPostCreate = true;
        return event;
    }

    public static ActivityLifeCycleEvent onStart(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onStart = true;
        return event;
    }

    public static ActivityLifeCycleEvent onResume(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onResume = true;
        return event;
    }

    public static ActivityLifeCycleEvent onPause(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onPause = true;
        return event;
    }

    public static ActivityLifeCycleEvent onStop(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onStop = true;
        return event;
    }

    public static ActivityLifeCycleEvent onDestroy(Class<? extends Activity> activityClass) {
        ActivityLifeCycleEvent event = new ActivityLifeCycleEvent(activityClass);
        event.onDestroy = true;
        return event;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getClass().getSimpleName());
        stringBuilder.append(".");
        if (onCreate) {
            stringBuilder.append("onCreate()");
        } else if (onPostCreate) {
            stringBuilder.append("onPostCreate()");
        } else if (onStart) {
            stringBuilder.append("onStart()");
        } else if (onResume) {
            stringBuilder.append("onResume()");
        } else if (onPause) {
            stringBuilder.append("onPause()");
        } else if (onStop) {
            stringBuilder.append("onStop()");
        } else if (onDestroy) {
            stringBuilder.append("onDestroy()");
        }
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
