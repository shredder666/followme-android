package com.shredder.common.eventbus.presenter;

public interface ChildPresenter<View>  {
    void start(View view);
    void stop();
}
