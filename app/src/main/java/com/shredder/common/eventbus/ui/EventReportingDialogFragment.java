package com.shredder.common.eventbus.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class EventReportingDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LifeCycleEventReporter.reportOnCreate(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        LifeCycleEventReporter.reportOnStart(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        LifeCycleEventReporter.reportOnAttach(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LifeCycleEventReporter.reportOnResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        LifeCycleEventReporter.reportOnPause(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LifeCycleEventReporter.reportOnDetach(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        LifeCycleEventReporter.reportOnStop(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LifeCycleEventReporter.reportOnDestroy(this);
    }
}
