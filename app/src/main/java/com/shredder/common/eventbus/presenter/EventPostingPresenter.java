package com.shredder.common.eventbus.presenter;

import com.shredder.common.eventbus.ShEventBus;

public abstract class EventPostingPresenter {

    protected void post(Object event) {
        ShEventBus.postEvent(event);
    }

    protected void postSticky(Object event) {
        ShEventBus.postStickyEvent(event);
    }

    protected <T> T getSticky(Class<T> eventClass) {
        return ShEventBus.getSticky(eventClass);
    }
}
